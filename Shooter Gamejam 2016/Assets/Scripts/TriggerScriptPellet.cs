﻿using UnityEngine;
using UnityEngine.Networking;

public class TriggerScriptPellet : NetworkBehaviour {

    [SerializeField]
    private GameObject vine;

    [SerializeField]
    private GameObject pellet;

    [SerializeField]
    private Vector3 normalVector;

    [SerializeField]
    private Vector3 normalVectorPos;
    
    private Quaternion toRotate ;

    GameObject localPlayer;
    GameObject[] players;
    void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject curPlayer in players)
        {

            if (curPlayer.layer.ToString() == "LocalPlayer")
            {
                localPlayer = curPlayer;
            }

        }
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            //Debug.Log("Destroy!!!!");
            CmdSpawn();
            
            Destroy(gameObject);
        }
        else if(collision.gameObject.tag == "Player")
        {
            CmdPlayerShot(collision.collider.name, 100);
            Destroy(gameObject);
        } 
    }

    [Command]
    void CmdPlayerShot(string _playerID, float damage)
    {
        Debug.Log(_playerID + "Has been shot");
        Player _player = GameManager.GetPlayer(_playerID);
        _player.RpcTakedamage(damage);
    }

    [Command]
    void CmdSpawn()
    {
        GameObject plant = Instantiate(vine, pellet.transform.position, toRotate) as GameObject;
        NetworkServer.Spawn(plant);
        GameObject tempRotation = Instantiate(vine, pellet.transform.position, Quaternion.identity) as GameObject;
        tempRotation.transform.LookAt(tempRotation.transform.position + normalVector);

        tempRotation.transform.Rotate(tempRotation.transform.right, 90, Space.World);
        toRotate = tempRotation.transform.rotation;
        plant.GetComponent<Vine>().myQuaternion = toRotate;
        Destroy(tempRotation);
  //      Cmdrotation(plant);
       
           
    }
    [Command]
    void Cmdrotation(GameObject plant)
    {
        plant.transform.LookAt(plant.transform.position + normalVector);
        Debug.DrawRay(plant.transform.position, plant.transform.up, Color.red, 200);
        plant.transform.Rotate(plant.transform.right, 90, Space.World);
        Debug.DrawRay(plant.transform.position, plant.transform.up, Color.blue, 200);
    }

    public void SetNormalVector (Vector3 normal)
    {
        normalVector = normal;

    }
}
        