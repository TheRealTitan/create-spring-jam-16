﻿using UnityEngine;
using System.Collections;
using System;

public class Mouse : MonoBehaviour {

    public bool lockCursor = true;
    private Vector2 mousePos;
    public Texture crosshair;
    [SerializeField]
    private int crosshairSize = 100;

    void OnGUI()
    {
        //mousePos = Event.current.mousePosition;
        if (crosshair != null) {
        GUI.DrawTexture(new Rect((Screen.width/2)- 10, (Screen.height/2)- 10, 20, 20), crosshair, ScaleMode.ScaleToFit);
        }
        else
        {
            Debug.Log("texture not found");
        }
    } 
    
}
