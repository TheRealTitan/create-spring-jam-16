﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;


public class GrenadeLauncher : NetworkBehaviour
{

    public float force;
    public GameObject grenade;
    public GameObject marker;
    public GameObject finalMarker;
    public int numOfMarkerPoints;
    GameObject realCamera;
    bool timer = true;

    List<GameObject> markerPointz;
    
    GameObject hitObject;
    float timeBetweenMarkers;
    bool hitStuff;
  
    // Use this for initialization
    void Start()
    {
        realCamera = this.transform.FindChild("Camera").gameObject;
        markerPointz = new List<GameObject>();
        for (int i = 0; i < numOfMarkerPoints; i++)
        {
            if (i == numOfMarkerPoints - 1)
            {
                GameObject tempMarker = Instantiate(finalMarker) as GameObject;
                markerPointz.Add(tempMarker);
            } else
            {
                GameObject tempMarker = Instantiate(marker) as GameObject;
                markerPointz.Add(tempMarker);
            }
          
           
        }

    }
    [Command]
   public void CmdSpawn(Vector3 position, Quaternion rotation, GameObject vine)
    {
        GameObject instVine = Instantiate(vine, position, rotation) as GameObject;
        NetworkServer.Spawn(instVine);
    }
	
    // Update is called once per frame
    void Update()
    {
       if(Input.GetMouseButton(1) && timer == true) { 
        hitStuff = false;
        hitObject = null;
          
            markerPointz [0].transform.position = realCamera.transform.position;
        
        Vector3 velocity = realCamera.transform.forward * force;

            for (int i = 1; i < numOfMarkerPoints; i++)
            {

                velocity += Physics.gravity * timeBetweenMarkers;
                if (velocity.magnitude == 0)
                    timeBetweenMarkers = 0;
                else
                {
                    timeBetweenMarkers = Time.fixedDeltaTime;
                }

                RaycastHit hit;

                if (hitStuff == false)
                {
                    if (Physics.Raycast(markerPointz[i - 1].transform.position, velocity * timeBetweenMarkers, out hit, velocity.magnitude * timeBetweenMarkers) && hit.collider.gameObject.tag == "Wall")
                    {
                    //    Debug.DrawRay(markerPointz[i - 1].transform.position, velocity * timeBetweenMarkers);
                        hitStuff = true;
                        markerPointz[i].transform.position = realCamera.transform.position;
                        markerPointz[numOfMarkerPoints - 1].transform.position = markerPointz[i - 1].transform.position + velocity.normalized * hit.distance * timeBetweenMarkers;
                  //      Debug.Log("Hits something!");
                        for (int j = i; j < numOfMarkerPoints - 1; j++)
                        {
                            markerPointz[j].transform.position = realCamera.transform.position;
                        }
                        break;
                    }
                    else
                    {
                        markerPointz[i].transform.position = markerPointz[i - 1].transform.position + velocity * timeBetweenMarkers;
                    }
                }
                else
                {
                    markerPointz[i].transform.position = markerPointz[i - 1].transform.position;
                }
            }
        } else 
            {
                for (int j = 0; j < numOfMarkerPoints ; j++)
                {
                    markerPointz[j].transform.position = new Vector3(-99999, -99999, -99999);
                }
            } 
            if (Input.GetMouseButtonUp(1) && timer == true)
        {
            CmdfireGrenade();
            Debug.Log("Try to fire grenade!");
        }   
    }
    [Command]
    void CmdfireGrenade()
    {
        GameObject tempGrenade = Instantiate(grenade, realCamera.transform.position, Quaternion.identity) as GameObject;
        tempGrenade.GetComponent<Rigidbody>().AddForce(realCamera.transform.forward * force, ForceMode.Impulse);
        tempGrenade.transform.eulerAngles = new Vector3(0, 0, 0);
        Physics.IgnoreCollision(tempGrenade.GetComponent<Collider>(), this.GetComponent<Collider>());
       
        tempGrenade.GetComponent<TriggerScriptGranata>().playerWhoShot = this.gameObject;
        NetworkServer.Spawn(tempGrenade);
        markerPointz[numOfMarkerPoints - 1].transform.position = new Vector3(-99999, -99999, -99999);
        timer = false;
        
        StartCoroutine(Cooldown(1));
    }
    
    IEnumerator Cooldown(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        timer = true;

    }
}
