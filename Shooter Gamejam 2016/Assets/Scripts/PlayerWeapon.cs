﻿using UnityEngine;

[System.Serializable]
public class PlayerWeapon {

    public string name = "Laser";

    public float damange = 10f;

    public float range = 200f;
}
