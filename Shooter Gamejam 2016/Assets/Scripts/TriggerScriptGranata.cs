﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class TriggerScriptGranata : NetworkBehaviour {

    [SerializeField]
    private GameObject vine;

    [SerializeField]
    private GameObject granata;

    [SerializeField]
    private Vector3 normalVector;

    [SerializeField]
    private Vector3 normalVectorPos;
    
    private Quaternion toRotate;

    public GameObject playerWhoShot;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Wall")
        {
            //Debug.Log("Destroy!!!!");
            normalVector = collision.contacts[0].normal;
            //GameObject plant = Instantiate(vine, this.transform.position, toRotate) as GameObject;
            GameObject tempRotation = Instantiate(vine, this.transform.position, Quaternion.identity) as GameObject;
            tempRotation.transform.LookAt(tempRotation.transform.position + normalVector);

            tempRotation.transform.Rotate(tempRotation.transform.right, 90, Space.World);
            toRotate = tempRotation.transform.rotation;
            playerWhoShot.GetComponent<GrenadeLauncher>().CmdSpawn(this.transform.position, toRotate, vine );
            Destroy(tempRotation);
            Destroy(gameObject);
        }
        else if (collision.gameObject.tag == "Player")
        {
            CmdPlayerShot(collision.collider.name, 100);
            Destroy(gameObject);
        }
    }

    [Command]
    void CmdPlayerShot(string _playerID, float damage)
    {
        Debug.Log(_playerID + "Has been blown up!");
        Player _player = GameManager.GetPlayer(_playerID) ; 
        _player.RpcTakedamage(damage); 
    }

     [Command]
    void CmdSpawn()
    {
        GameObject plant = Instantiate(vine, this.transform.position, toRotate) as GameObject;
        NetworkServer.Spawn(plant);
        GameObject tempRotation = Instantiate(vine, this.transform.position, Quaternion.identity) as GameObject;
        tempRotation.transform.LookAt(tempRotation.transform.position + normalVector);

        tempRotation.transform.Rotate(tempRotation.transform.right, 90, Space.World);
        toRotate = tempRotation.transform.rotation;
        plant.GetComponent<Vine>().myQuaternion = toRotate;
        Destroy(tempRotation); 
        //      Cmdrotation(plant);


    }
    [Command]
    void Cmdrotation(GameObject plant)
    {
        plant.transform.LookAt(plant.transform.position + normalVector);
        Debug.DrawRay(plant.transform.position, plant.transform.up, Color.red, 200);
        plant.transform.Rotate(plant.transform.right, 90, Space.World);
        Debug.DrawRay(plant.transform.position, plant.transform.up, Color.blue, 200);
    }

    public void SetNormalVector(Vector3 normal)
    {
        normalVector = normal;

    }
}
