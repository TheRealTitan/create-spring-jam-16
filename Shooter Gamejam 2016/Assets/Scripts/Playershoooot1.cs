﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class Playershoooot1 : NetworkBehaviour {

        private const string PLAYER_TAG = "Player";
        public PlayerWeapon weapon;

        [SerializeField]
        private GameObject laser;

        [SerializeField]
        private GameObject pellet;

        [SerializeField]
        private PelletMovement PM;

        [SerializeField]
        private Camera cam;

        [SerializeField]
        private LayerMask mask;

        [SerializeField]
        private Quaternion rotation;

        void Start()
        {
            cam = GetComponentInChildren<Camera>();
            if (cam == null)
            {
                Debug.LogError("PlayerShoot: No camera referenced");
                this.enabled = false;
            }
        }

        void Update()
        {
            //Debug.DrawRay(cam.transform.position, cam.transform.forward*100,Color.red,10000);
            if (Input.GetButtonDown("Fire1"))
            {
                Debug.Log("clicked mouse");
                Shoot1();
            }

        }
        void Shoot1()
        {
            //Instantiate(pellet, cam.transform.position, rotation);
            RaycastHit _hit;
            //Debug.DrawRay(cam.transform.position, cam.transform.forward*100, Color.red, 1000);
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out _hit,1000, mask))
            {
                
                Debug.Log("It hit something");
                if (_hit.collider.tag == PLAYER_TAG)
                {
                    Debug.Log("inside");
                    CmdPlayerShot(_hit.collider.name, weapon.damange);
                }
            }
        }
        [Command]
        void CmdPlayerShot(string _playerID, float damage)
        {
            Debug.Log(_playerID + "Has been shot");
            Player _player = GameManager.GetPlayer(_playerID);
            _player.RpcTakedamage(damage);
        }
    }

