﻿using UnityEngine;
using System.Collections;
//[RequireComponent(typeof(PlayerMotor))]
//[RequireComponent(typeof(PlayerMotor))]
public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private float speed = 5f;
    [SerializeField]
    private float lookSensitivity = 3f;

    [SerializeField]
    private float trusterForce = 1000f;

    private PlayerMotor motor;

    // Use this for initialization
    void Start()
    {
        Cursor.lockState = CursorLockMode.Confined;
        motor = GetComponent<PlayerMotor>();
    }

    // Update is called once per frame
    void Update()
    {
        //calculate our movement velocity as a 3D Vector
        float _xMov = Input.GetAxisRaw("Horizontal"); // goes between -1 to 1
        float _zMov = Input.GetAxisRaw("Vertical"); // goes between -1 to 1

        Vector3 _movHorizontal = transform.right * _xMov; //(1, 0, 0) * xMov
        Vector3 _moveVertical = transform.forward * _zMov;  //(0, 0, 1) * zMov

        // final movement vector
        Vector3 _velocity = (_movHorizontal + _moveVertical).normalized * speed;

        //apply movement
        motor.Move(_velocity);

        //Calculate rotation as a 3D vector (turning around)
        float _yRot = Input.GetAxisRaw("Mouse X");

        Vector3 _rotation = new Vector3(0f, _yRot, 0f) * lookSensitivity;

        //apply rotation
        motor.Rotate(_rotation);

        //Calculate rotation as a 3D vector (turning around)
        float _xRot = Input.GetAxisRaw("Mouse Y");

        float _camerarotationX = _xRot * lookSensitivity;

        //apply rotation
        motor.RotateCamera(_camerarotationX);

        //calculate thrusterforce
        Vector3 _thrusterForce = Vector3.zero;

        if (Input.GetButton("Jump"))
        {
            _thrusterForce = Vector3.up * trusterForce;
        }

        // Apply the thruster force
        motor.ApplyThruster(_thrusterForce);
    }
}
