﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class Vine : NetworkBehaviour {
    [SyncVar]
    public Quaternion myQuaternion;


    // Use this for initialization
    void Start () {
        transform.rotation = myQuaternion;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
