﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;
//May need changes in PlayerSetup
public class playerShoot : NetworkBehaviour {

    public PlayerWeapon weapon;

    [SerializeField]
    private GameObject laser;

    [SerializeField]
    private TriggerScriptPellet TSP;

    [SerializeField]
    private GameObject pellet;

    [SerializeField]
    private Camera cam;

    [SerializeField]
    private LayerMask mask;

    [SerializeField]
    private Quaternion rotation;

    [SerializeField]
    private bool timer = true;

    [SerializeField]
    private float laserRange;

    [SerializeField]
    private float gunLength = 1.2f;

    [SerializeField]
    private float laserRadius = 0.04f;

    void Start()
    {
        if (cam == null)
        {
            //Debug.LogError("PlayerShoot: No camera referenced");
            this.enabled = false;
        }
        
    }

    void Update()
    {
        //Debug.DrawRay(cam.transform.position, cam.transform.forward, Color.red, 100);
        /*
        if (Input.GetButtonDown("Fire2"))
        {
            //Debug.Log("clicked mouse");

            Shoot1();
        }
        */
        if (Input.GetButtonDown("Fire1") && timer == true)
        {
            RaycastHit ___hit;
            if (Physics.Raycast(cam.transform.position, cam.transform.forward, out ___hit, 9999,mask))
            {
                //Debug.Log("hit!   Normal = " + ___hit.normal + " Name: " + ___hit.collider.gameObject.name);
              //  Debug.DrawLine(this.cam.transform.position, ___hit.point, Color.red, 4000);
                Debug.DrawRay(___hit.point, ___hit.normal, Color.green, 4000);    
            }

            CmdShoot(cam.transform.position + cam.transform.forward, rotation,___hit.normal);
            //TSP = GameObject.Find("Pellet(Clone)").GetComponent<TriggerScriptPellet>();
            //TSP.SetCameraStartPosition(cam.transform.position);
            timer = false;

           /* TSP = GameObject.Find("Pellet(Clone)").GetComponent<TriggerScriptPellet>();
            TSP.SetNormalVector(___hit.normal);
            */
            StartCoroutine(Cooldown(1));
            
        }
    }
    void Shoot1()
    {
        laserRange = weapon.range;
       /* RaycastHit _hit;
        if(Physics.Raycast(cam.transform.position, cam.transform.forward, out _hit, 9999, mask))
        {
            Vector3 rayLine = _hit.point - cam.transform.position;
            if (rayLine.magnitude < laserRange)
            {
                laserRange = rayLine.magnitude;
                //Debug.Log("Laser range: " + laserRange);
            }
        }
        */
        GameObject beam = Instantiate(laser, cam.transform.position + (cam.transform.forward * (laserRange / 2 + gunLength)), Quaternion.LookRotation(cam.transform.forward, Vector3.up)) as GameObject;
        beam.transform.Rotate(Vector3.right, 90);
        beam.transform.localScale = new Vector3(laserRadius, laserRange, laserRadius);
        beam.GetComponent<PelletMovement>().direction = cam.transform.forward;

        //laser.transform.localScale = new Vector3(0.01f, _hit.distance/2, 0.01f);
        //Instantiate(laser,cam.transform.position + cam.transform.forward,rotation);
    }

    [Command]
    void CmdShoot(Vector3 pos, Quaternion rot, Vector3 normal)
    {

        GameObject clone = (GameObject)Instantiate(pellet, pos, rot);
        clone.GetComponent<PelletMovement>().direction = cam.transform.forward;
        clone.GetComponent<TriggerScriptPellet>().SetNormalVector(normal);
        NetworkServer.Spawn(clone);
    }

    IEnumerator Cooldown(int seconds)
    {
        yield return new WaitForSeconds(seconds);
        timer = true;
       
    }
}
