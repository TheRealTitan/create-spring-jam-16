﻿using UnityEngine;
using UnityEngine.Networking;

public class PelletMovement : NetworkBehaviour {

    [SerializeField]
    public Vector3 direction;

    [SerializeField]
    private float velocity = 1f;

    [SerializeField]
    private GameObject pellet;

    public string id;
    /*
    void Start()
    {
        GameObject[] objects;
        objects = GameObject.FindGameObjectsWithTag("Player");
        foreach (GameObject go in objects)
        {
            Debug.Log(LayerMask.LayerToName(go.layer));
            if (LayerMask.LayerToName(go.layer) == "LocalPlayer");
            {
                direction = GameObject.Find("Player 1").transform.GetChild(0).transform.forward;
            }
        }
         
        //direction = GameObject.Find("Player 1").GetComponentInChildren<Camera>().transform.forward;
        Debug.Log("direction: " + direction);
    }
    */
    void FixedUpdate()
    {
        Move();
    }

    public void Move()
    {
        //Debug.Log("velocity: " + startVelocity + "     camera: " + __hit.point);
        transform.position += velocity * direction * Time.deltaTime;
    }
    
}
