﻿using UnityEngine;
using System.Collections;

// The GameObject requires a Rigidbody component
[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour {

    [SerializeField]
    private Camera cam;

    private Vector3 velocity = Vector3.zero;
    private Vector3 rotation = Vector3.zero;
    private float cameraRotationX = 0f;
    private float currentCameraRotationX = 0f;
    private Vector3 thrusterForce = Vector3.zero;
    [SerializeField]
    private float hoverDistance = 2f;
    [SerializeField]
    private float hoverForce = 1000f;
    [SerializeField]
    private float cameraRotationLimit = 85f;

    private Rigidbody rb;
    private SphereCollider sc;

    // Use this for initialization
    void Start() {
        rb = GetComponent<Rigidbody>();
        sc = GetComponent<SphereCollider>();
    }
    // run every physics iteration
    void FixedUpdate()
    {
        Hover();
        PerformMovement();
        PerformRotation();
    }

    void PerformMovement()
    {
        if (velocity != Vector3.zero)
            rb.MovePosition(transform.position + velocity * Time.fixedDeltaTime);

        if (thrusterForce != Vector3.zero)
        {
            rb.AddForce(thrusterForce * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
    }
    void PerformRotation()
    {
        rb.MoveRotation(transform.rotation * Quaternion.Euler(rotation));
        if (cam != null)
        {
            // set our rotation and clamp it
            currentCameraRotationX -= cameraRotationX;
            currentCameraRotationX = Mathf.Clamp(currentCameraRotationX, -cameraRotationLimit, cameraRotationLimit);
            // apply our rotation to the transform of our camera
            cam.transform.localEulerAngles = new Vector3(currentCameraRotationX, 0f, 0f);
        }
    }
    public void Move(Vector3 _velocity)
    {
        velocity = _velocity;
    }
    public void Rotate(Vector3 _rotation)
    {
        rotation = _rotation;
    }
    public void RotateCamera(float _cameraRotationX)
    {
        cameraRotationX = _cameraRotationX;
    }
    public void ApplyThruster(Vector3 _thrusterForce)
    {
        thrusterForce = _thrusterForce;
    }
    public void Hover()
    {
        //Debug.DrawRay(transform.position, Vector3.down, Color.white, hoverDistance);
        if (Physics.Raycast(transform.position, Vector3.down, hoverDistance))
       {
            //Debug.Log("inside");
            rb.AddForce((Vector3.up * hoverForce) * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
        else
        {
            //rb.AddForce((Vector3.down * hoverForce) * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
    }
}
        