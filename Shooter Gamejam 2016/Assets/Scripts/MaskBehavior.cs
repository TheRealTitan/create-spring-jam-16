﻿using UnityEngine;
using System.Collections;

public class MaskBehavior : MonoBehaviour
{
	float timeLeft = 2f;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
		this.transform.Translate (transform.up * Time.deltaTime / 10);
		timeLeft -= Time.deltaTime;
		if (timeLeft <= 0)
			Destroy (gameObject);
	}
}
