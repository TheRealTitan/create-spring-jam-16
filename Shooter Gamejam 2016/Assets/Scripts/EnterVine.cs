﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class EnterVine : NetworkBehaviour {

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Vine")
        {
            CmdVine(this.GetComponent<Collider>().name, 100);
        }
    }
    [Command]
    void CmdVine(string _playerID, float damage)
    {
        Debug.Log(_playerID + "Vine killed you");
        Player _player = GameManager.GetPlayer(_playerID);
        _player.RpcTakedamage(damage);
    }
}

 
