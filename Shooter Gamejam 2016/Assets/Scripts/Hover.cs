﻿using UnityEngine;
using System.Collections;

public class Hover : MonoBehaviour {
    
    private Rigidbody rb;
    [SerializeField]
    private float hoverspeed = 100f;
    Collider hit = null;
    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
    }

    void OnCollisionEnter(Collision collision)
    {
            Debug.Log("hitting something");
            rb.AddForce(Vector3.up * hoverspeed * Time.fixedDeltaTime, ForceMode.Acceleration);
    }
    void Update()
    {
    }
}
